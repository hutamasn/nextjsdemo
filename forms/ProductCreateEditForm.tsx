import React from "react";
import v8n from "v8n";

export interface ProductCreateEditFormData {
    name: string;
    price: number | '';
    quantity: number | '';
}

export class ProductCreateEditForm extends React.Component<{
    form: ProductCreateEditFormData,
    onChange: (ProductCreateEditFormData) => void,
    onSubmit: (ProductCreateEditFormData) => Promise<void>
}, {
    error: {
        name: string,
        price: string,
        quantity: string
    }
}>{

    constructor(props) {
        super(props);
        this.state={
            error:{
                name:'',
                price:'',
                quantity: ''
            }
        }
    }

    onNameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        const form = this.props.form;
        form.name = e.target.value;
        
        this.validate(form);
        this.props.onChange(form);
    }

    onPriceChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        const form = this.props.form;
        const value = parseInt(e.target.value);

        if (isNaN(value)) {
            form.price = '';
        }
        else {
            form.price = value;
        }

        this.validate(form);
        this.props.onChange(form);
    }

    onQuantityChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        const form = this.props.form;
        const value = parseInt(e.target.value);

        if (isNaN(value)) {
            form.quantity = '';
        }
        else {
            form.quantity = value;
        }

        this.validate(form);
        this.props.onChange(form);
    }

    onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const form = this.props.form;

        if(this.validate(form) === false)
        {
            return ;
        }
        
        await this.props.onSubmit(form)
    }

    validate = ( formData : ProductCreateEditFormData) => {
        const error = {
            name: '',
            price: '',
            quantity: ''
        };

        const validateName = v8n()
            .string()
            .not
            .empty()
            .maxLength(255)
            .testAll(formData.name);
        
        const validateNameError = validateName[0];

        if(validateNameError)
        {
            error.name = validateNameError.rule.name;
        }

        const validatePrice = v8n()
            .number().not.empty()
            .between( 10_000 , 5_000_000)
            .testAll(formData.price);
        
        const validatePriceError = validatePrice[0];

        if(validatePriceError)
        {
            error.price = validatePriceError.rule.name;
        }

        const validateQuantity = v8n()
            .number().not.empty()
            .between( 1 , 1000)
            .testAll(formData.quantity);

        const validateQuantityError = validateQuantity[0];

        if(validateQuantityError)
        {
            error.quantity = validateQuantityError.rule.name;
        }

        this.setState({
            error: error 
        });

        

        for( let key in error )
        {
            if(error[key])
            {
               return false;
            }
        }

        return true ;
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <div>
                        <label htmlFor="name">Name</label>
                        <input id="name" className="form-control" value={this.props.form.name} onChange={this.onNameChanged}></input>
                        {this.state.error.name && <span className="text-danger small">{this.state.error.name}</span>}
                    </div>

                    <div>
                        <label htmlFor="price">Price</label>
                        <input id="price" className="form-control" value={this.props.form.price} onChange={this.onPriceChanged}></input>
                        {this.state.error.price && <span className="text-danger small">{this.state.error.price}</span>}
                    </div>

                    <div>
                        <label htmlFor="quantity">Quantity</label>
                        <input id="quantity" className="form-control" value={this.props.form.quantity} onChange={this.onQuantityChanged}></input>
                        {this.state.error.quantity && <span className="text-danger small">{this.state.error.quantity}</span>}
                    </div>

                    <div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        );
    }
}
