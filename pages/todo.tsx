import { makeObservable, observable } from "mobx";
import { observer } from "mobx-react";
import React, { ChangeEvent } from "react";
import { Layout } from './shared/Layout'


/** Function Component */
interface TodoListItem{
    name: string;
    checked: boolean;
}

interface TodoState{
    inputText: string;
    todoList: TodoListItem[];
}

const TodoItem: React.FunctionComponent<{
    item: TodoListItem
    onRemoveButtonClick?: (item: TodoListItem) => void
}> = (props) => {
            
    const onClick = () =>{
        if(props.onRemoveButtonClick){
            props.onRemoveButtonClick(props.item);
        }
    }

    return (
        <div>
            <button type = "button" onClick = {onClick}>Remove</button>
            {props.item.name}
        </div>
    )
};

const TodoList: React.FunctionComponent<{
    list: TodoListItem[]
    onChange?: (newList: TodoListItem[]) => void
}> = (props) => {
    const onRemoveButtonClicked = (item: TodoListItem) =>{
        const index = props.list.findIndex(Q => Q === item);
        props.list.splice(index,1);
        if(props.onChange){
            props.onChange(props.list);
        }

    };
    const listItems = props.list.map (Q => <TodoItem item ={Q} onRemoveButtonClick = {onRemoveButtonClicked}></TodoItem>);
    return(
        <div>
            {listItems}
        </div>
    );
}


// Class Component
class Todo extends React.Component<{}, TodoState>{
    // list: TodoListItem[] = [];

    // inputText: string = '';

    onInputTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            inputText: e.target.value
        });
    };

    onListChanged = (newList: TodoListItem[]) => {
        this.setState({
            todoList: newList
        });
    }

    onAddButtonClick = () => {
        const todoList = this.state.todoList;
        todoList.push({
            name: this.state.inputText,
            checked: false
        });
        this.setState({
            inputText: '',
            todoList: todoList
        });
    };

    constructor(props){
        super(props);
        this.state = {
            inputText: '',
            todoList: []
        };
        // makeObservable(this,{
        //     list: observable
        // });
    }

    render(){
        return(
            <div>
                <div>
                    <input value = {this.state.inputText} onChange = {this.onInputTextChange}></input>
                    <button type = "button" onClick = {this.onAddButtonClick}>Add</button>
                </div>
                <TodoList list = {this.state.todoList} onChange = {this.onListChanged}></TodoList>
            </div>
        );
    }


}

export default function TodoPage(){
    return (
        <Layout title = "To-Do List">
            <Todo></Todo>
        </Layout>
    );
}
