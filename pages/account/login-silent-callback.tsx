import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { UserManagerFactory } from "../../services/UserManagerFactory";
import { Layout } from "../shared/Layout";

const SilentLoginCallback: React.FunctionComponent<{}> = () => {


    const router = useRouter();

    const handleSilentLogin = async () => {
        //
        const userManager = UserManagerFactory();
        

        if (await userManager.getUser()) {
            router.push('/');
        }

        try {
            const user = await userManager.signinSilentCallback(location.href);
            // const user= await userManager.getUser();

        } catch (err) {
            //gabisa login
            router.push('/account/login');
        }

    }

    useEffect(() => {
        handleSilentLogin();
    });

    return (
        <div>
            Loading...
        </div>
    );
}

export default function LoginPage() {
    return (
        <Layout title="Login">
            <SilentLoginCallback></SilentLoginCallback>
        </Layout>
    )
}
