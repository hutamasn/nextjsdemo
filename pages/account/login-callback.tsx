import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { UserManagerFactory } from "../../services/UserManagerFactory";
import { Layout } from "../shared/Layout";

const LoginCallback: React.FunctionComponent<{}> = () => {

  
    const router = useRouter();

    const handleLogin = async() =>{
        //
        const userManager = UserManagerFactory();
        let success = false;
        if(await userManager.getUser()){
            router.push('/');
        }

        try{
            const user = await userManager.signinRedirectCallback(location.href);
             // const user= await userManager.getUser();
            console.log(user)
            success= true;
        }catch(err){
            //gabisa login
            router.push('/account/login');
        }
       
        if(success){
            router.push('/');
        }

        //redirect ke homepage 
    }

    useEffect(()=>{
       handleLogin();
    });

    return (
        <div>
            Loading...
        </div>
    );
}

export default function LoginPage() {
    return (
        <Layout title="Login">
            <LoginCallback></LoginCallback>
        </Layout>
    )
}