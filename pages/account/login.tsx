import React from "react";
import { Layout } from "../shared/Layout";
import { UserManagerFactory } from "../../services/UserManagerFactory";

export function Login(){

    // PKCE = Proof of Key Exchange
    const tryLoginUsingAccelistSSO = async() => {
        const userManager = UserManagerFactory();
        await userManager.signinRedirect();
        //http://localhost:3000/account/login-callback
        //?state=a0f96d3c539f492db107c1ea94bb107f
        //&session_state=824a19ae-1dc4-41dd-840b-46b7384cb4d7
        //&code=2a064845-1597-4132-a719-d8b2c1d6c24a.824a19ae-1dc4-41dd-840b-46b7384cb4d7.31a5e097-9062-4528-9212-966d216ac635
    }

    return(
        <div>
            <button className = "btn btn-primary" onClick={tryLoginUsingAccelistSSO}>
                Login
            </button>
        </div>
    );
}

export default function LoginPage(){
    return (
            <Layout title = "Login">
                <Login></Login>
            </Layout>
    )
} 