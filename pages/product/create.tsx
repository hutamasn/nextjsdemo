import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import React from "react";
import { ProductClient } from "../../api/shop-api";
import { ProductCreateEditForm, ProductCreateEditFormData } from "../../forms/ProductCreateEditForm";
import { Layout } from "../shared/Layout";;

class CreateProduct extends React.Component<{} , {
    form: ProductCreateEditFormData
}>{

    constructor(props){
        super(props);
        this.state = {
            form: {
                name: '',
                price: 0,
                quantity: 0
            }
        }

    }

    onFormDataChange = ( formData : ProductCreateEditFormData) =>  {
        // const form = this.state.form; 
        // form.name = formData.name;
        // form.price = formData.price;
        // this.setState({
        //     form: form 
        // });
        // Bisa di ubah jadi di bawah

        this.setState({
            form: {...formData} 
        });
    }

    onSubmitForm = async (formData:ProductCreateEditFormData) => {
        const client = new ProductClient('https://localhost:44324');
        let intPrice = 0 ; 
        let intQuantity = 0;

        if(formData.price)
        {
            intPrice=formData.price;
        }

        if(formData.quantity)
        {
            intQuantity = formData.quantity;
        }

        await client.post({
            name: formData.name,
            price: intPrice,
            quantity: intQuantity
        });

        this.setState({
            form:{
                name: '',
                price: 0,
                quantity: 0
            }
        });
    }


    render(){
        return(
            <div>
                <h1>Create Product</h1>

                <Link href="/product">
                    <a>
                        <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        Return to index
                    </a>
                </Link>

                <ProductCreateEditForm form={this.state.form} onChange={this.onFormDataChange} onSubmit={this.onSubmitForm} >

                </ProductCreateEditForm>
            </div>
        );
    }
}

export default function CreateProductPage(){
    return <Layout title="Create Product">
        <CreateProduct></CreateProduct>
    </Layout>
}