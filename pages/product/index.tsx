import axios from 'axios';
import Link from 'next/link';
import React from "react";
import Swal, { SweetAlertResult } from 'sweetalert2';
import { ProductListItems, ProductClient } from "../../api/shop-api";
import { Layout } from '../shared/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';

// DELETE FUNCTION
const DeleteProductButton: React.FunctionComponent<{
    productID: string,
    name: string | undefined,
    onDeleted?: () => void
}> = (props) => {

    const onClick = async () => {
        const confirm = await Swal.fire<SweetAlertResult>({
            title: 'Confirm delete?',
            text: `Delete customer ${props.name}? This action cannot be undone.`,
            icon: 'warning',
            confirmButtonColor: '#dc3545',
            showCancelButton: true,
            confirmButtonText: 'Delete'
        });

        if (confirm.isConfirmed === false) {
            return;
        }

        const client = new ProductClient('https://localhost:44324');
        await client.delete(props.productID);

        Swal.fire({
            toast: true,
            timerProgressBar: true,
            timer: 5000,
            showConfirmButton: false,
            position: 'bottom-right',
            icon: 'success',
            title: 'Successfully deleted customer: ' + props.name
        });

        if (props.onDeleted) {
            props.onDeleted();
        }
    };

    return (
        <button type="button" onClick={onClick} className="btn btn-danger btn-sm">
            <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
        </button>
    );
};

//EDIT FUNTION
const EditProductButton: React.FunctionComponent<{
    productID: string}> = (props) => {
    return (
        <Link href={`product/edit/${props.productID}`}>
            <a href="/product/edit" className="btn btn-info btn-sm">
                <FontAwesomeIcon icon={faEdit}></FontAwesomeIcon>
            </a>
        </Link>
    );
};

//LIST PTODUCT FUNCTION
const ProductListItemRows: React.FunctionComponent<{
    products: ProductListItems[],
    onChanged?: () => void
}> = (props) => {

    const rows = props.products.map(Q =>

        <tr key={Q.productID}>
            <td>{Q.productID}</td>
            <td>{Q.name}</td>
            <td>{Q.price}</td>
            <td>{Q.quantity}</td>
            <td>
                {/* EDIT */}
                <EditProductButton productID={Q.productID}></EditProductButton>
                {/* DELETE DONE*/}
                <DeleteProductButton productID={Q.productID} name={Q.name} onDeleted={props.onChanged}></DeleteProductButton>
            </td>
        </tr>
    );

    return <tbody>{rows}</tbody>;
}

//MAIN
class Product extends React.Component<{}, {
    products: ProductListItems[]
}> {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        };
    }

    async componentDidMount() {
        await this.reloadProductData();
    }

    reloadProductData = async () => {
        const client = new ProductClient('https://localhost:44324');
        const data = await client.getAll();
        this.setState({
            products: data
        });
    }

    render() {
        return (
            <div>
                <h1>Manage Product</h1>

                {/*ADD PRODUCT*/}
                <p>
                    <Link href="/product/create">
                        <a className="btn btn-primary">
                            <span className="me-2">
                                <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                            </span>
                            Add New Product
                        </a>
                    </Link>
                </p>

                <table className="table table-hover table-striped">
                    <thead className="bg-dark text-light">
                        <tr>
                            <th>Product ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th></th>
                        </tr>
                    </thead>
                    <ProductListItemRows products={this.state.products} onChanged={this.reloadProductData}></ProductListItemRows>
                </table>
            </div>
        );
    }
}

export default function ProductPage() {
    return (
        <Layout title="Product">
            <Product></Product>
        </Layout>
    );
}
