// import React from 'react';
// import Link from 'next/link';
// import { GetServerSideProps } from 'next';
// import { ProductClient, ProductListItems } from "../../../api/shop-api";
// import { Router, useRouter, withRouter } from 'next/router';
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faArrowLeft, faChevronUp, faSpinner } from "@fortawesome/free-solid-svg-icons";
// import { Layout } from "../../shared/Layout";
// import Swal from 'sweetalert2';

// class EditProduct extends React.Component<{
//     id: string | undefined
// }, {
//     id: string,
//     form: {
//         name: string,
//         price: number,
//         quantity: number
//     },
//     errors: {
//         name: string,
//         price: string,
//         quantity: string
//     },
//     dirty: {
//         name: boolean,
//         price: boolean,
//         quantity: boolean
//     },
//     busy: boolean
// }>{
//     constructor(props) {
//         super(props);

//         this.state = {
//             id: props.id,
//             form: {
//                 name: '',
//                 price: 0,
//                 quantity: 0
//             },
//             errors: {
//                 name: '',
//                 price: '',
//                 quantity: ''
//             },
//             dirty: {
//                 name: false,
//                 price: false,
//                 quantity: false
//             },
//             busy: false
//         }
//     }

//     async componentDidMount() {
//         await this.loadProductData();
//     }

//     loadProductData = async () => {
//         const client = new ProductClient('https://localhost:44324');

//         const data = await client.get(this.state.id);

//         this.setState({
//             form: {
//                 name: data.name ?? '',
//                 price: data.price ?? '',
//                 quantity: data.quantity ?? ''
//             }
//         });
//     }

//     onSubmit = async (e: React.SyntheticEvent) => {
//         e.preventDefault();

//         await this.validate();

//         let hasError = false;
//         for (let key in this.state.errors) {
//             if (this.state.errors[key]) {
//                 hasError = true;
//             }
//         }

//         const dirty = this.state.dirty;

//         for (let key in dirty) {
//             dirty[key] = true;
//         }

//         this.setState({
//             dirty: dirty
//         });

//         if (hasError) {
//             return;
//         }

//         const form = this.state.form;

//         this.setState({
//             busy: true
//         });

//         try {
//             const client = new ProductClient('https://localhost:44324');
//             // The method should be named update....
//             await client.post2(this.state.id, {
//                 name: form.name,
//                 price: form.price,
//                 quantity: form.quantity
//             });
//         } catch (error) {
//             console.error(error);
//             Swal.fire({
//                 title: 'Submit Failed',
//                 text: 'An error has occurred. Please try again or contact an administrator',
//                 icon: 'error'
//             });
//         }
//         finally {
//             this.setState({
//                 busy: false
//             });
//         }

//         this.setState({
//             dirty: {
//                 name: false,
//                 price: false,
//                 quantity: false
//             }
//         })

//         Swal.fire({
//             title: 'Success!',
//             text: 'Successfully updated Product: ' + form.name,
//             icon: 'success'
//         });
//     }

//     hasErrorClassName(hasError: string | boolean, isDirty: boolean) {
//         if (isDirty) {
//             if (hasError) {
//                 return 'is-invalid';
//             } else {
//                 return 'is-valid';
//             }
//         }

//         return '';
//     }

//     onNameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
//         const form = this.state.form;
//         form.name = e.target.value;

//         const dirty = this.state.dirty;
//         dirty.name = true;

//         this.setState({
//             form: form,
//             dirty: dirty
//         });

//         this.validate();
//     }

//     onPriceChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
//         const form = this.state.form;
//         form.price = parseInt(e.target.value);

//         const dirty = this.state.dirty;
//         dirty.price = true;

//         this.setState({
//             form: form,
//             dirty: dirty
//         });

//         this.validate();
//     }

//     onQuantityChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
//         const form = this.state.form;
//         form.quantity = parseInt(e.target.value);

//         const dirty = this.state.dirty;
//         dirty.quantity = true;

//         this.setState({
//             form: form,
//             dirty: dirty
//         });

//         this.validate();
//     }

//     async validate() {
//         const errors = {
//             name: '',
//             price: '',
//             quantity: ''
//         };

//         // harusnya ada librarynya untuk validasi biar lebih enak...
//         if (!this.state.form.name) {
//             errors.name = 'Name field is required.';
//         }
//         if (!this.state.form.price) {
//             errors.price = 'Price cannot be alphabet or empty';
//         } 
//         if (!this.state.form.quantity) {
//             errors.quantity = 'Quantity cannot be alphabet or empty';
//         } 

//         await new Promise<void>((ok, reject) => {
//             this.setState({
//                 errors: errors
//             }, () => {
//                 ok();
//             });
//         });
//     }

//     getSubmitButtonIcon() {
//         if (this.state.busy) {
//             return <FontAwesomeIcon icon={faSpinner} pulse></FontAwesomeIcon>
//         } else {
//             return <FontAwesomeIcon icon={faChevronUp}></FontAwesomeIcon>
//         }
//     }

//     render() {
//         return <div>
//             <h1>Edit Product</h1>
//             <p>
//                 <Link href="/product">
//                     <a>
//                         <span className="me-2">
//                             <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
//                         </span>
//                     Return to index
//                 </a>
//                 </Link>
//             </p>
//             <form onSubmit={this.onSubmit}>
//                 <fieldset disabled={this.state.busy}>
//                     <div className="mb-3">
//                         <label className="fw-bold" htmlFor="name">Name</label>
//                         <input id="name" className={'form-control ' + this.hasErrorClassName(this.state.errors.name, this.state.dirty.name)}
//                             value={this.state.form.name} onChange={this.onNameChanged}></input>
//                         {this.state.errors.name && <span className="text-danger small">{this.state.errors.name}</span>}
//                     </div>
//                     <div className="mb-3">
//                         <label className="fw-bold" htmlFor="price">Price</label>
//                         <input id="price" className={'form-control ' + this.hasErrorClassName(this.state.errors.price, this.state.dirty.price)}
//                             value={this.state.form.price} onChange={this.onPriceChanged}></input>
//                         {this.state.errors.price && <span className="text-danger small">{this.state.errors.price}</span>}
//                     </div>
//                     <div className="mb-3">
//                         <label className="fw-bold" htmlFor="quantity">Quantity</label>
//                         <input id="quantity" className={'form-control ' + this.hasErrorClassName(this.state.errors.quantity, this.state.dirty.quantity)}
//                             value={this.state.form.quantity} onChange={this.onQuantityChanged}></input>
//                         {this.state.errors.quantity && <span className="text-danger small">{this.state.errors.quantity}</span>}
//                     </div>
//                     <div className="mb-3">
//                         <button type="submit" className="btn btn-primary">
//                             <span className="me-2">
//                                 {this.getSubmitButtonIcon()}
//                             </span>
//                             Submit
//                         </button>
//                     </div>
//                 </fieldset>
//             </form>
//         </div>;
//     }
// }

// function EditProductPage() {

//     const router = useRouter();

//     const { id } = router.query;

//     console.log('id parameter', id);

//     return <Layout title="Edit Customer">
//         <EditProduct id={id?.toString()}></EditProduct>
//     </Layout>;
// }

// export const getServerSideProps: GetServerSideProps = async (context) => {
//     return {
//         props: {
//         }
//     };
// }

// export default EditProductPage;

import { GetServerSideProps } from "next";
import React from "react";
import { ProductClient } from "../../../api/shop-api";
import { ProductCreateEditForm, ProductCreateEditFormData } from "../../../forms/ProductCreateEditForm";
import { Layout } from "../../shared/Layout";
import ErrorPage from "next/error";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

interface EditProductRouteProps{
    id:string
}

class EditProduct extends React.Component<EditProductRouteProps,{
    form: ProductCreateEditFormData,
    notFound: boolean
}>{
    constructor(props){
        super(props);
        this.state = {
            form:{
                name:'',
                price: 0,
                quantity: 0
            },
            notFound: false
        }
        
    }

    onFormEditDataChange = (formData: ProductCreateEditFormData) => {
        this.setState({
            form:{...formData}
        });
    }
    
    async componentDidMount(){
        await this.loadProductData();
    }

    loadProductData = async () =>{
        const client = new ProductClient('https://localhost:44324');
        
        try {
            const data = await client.get(this.props.id);
            this.setState({
                form:{
                    name: data.name ?? '',
                    price: data.price ?? '',
                    quantity: data.quantity ?? ''
                }
            })
        } catch (err) {
            this.setState({
                notFound: true
            })
        }
    }

    onSubmitFormData = async (formData : ProductCreateEditFormData) => {
        const client = new ProductClient('https://localhost:44324');
        
        let intPrice = 0 ; 
        let intQuantity = 0;

        if(formData.price)
        {
            intPrice=formData.price;
        }

        if(formData.quantity)
        {
            intQuantity=formData.quantity;
        }

        await client.post2( this.props.id,{
            name: formData.name,
            price: intPrice,
            quantity: intQuantity
        });

        // this.setState({
        //     form:{
        //         name: formData.name,
        //         price: formData.price
        //     }
        // });
    }

    render(){
        if(this.state.notFound === true)
        {
            return <ErrorPage statusCode={404}></ErrorPage>;
        }
        return(
            <div>
                <Link href="/product">
                    <a>
                        <span className="me-2">
                            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        </span>
                    Return to index
                    </a>
                </Link>

                <ProductCreateEditForm onChange={this.onFormEditDataChange} 
                onSubmit={this.onSubmitFormData} 
                form={this.state.form}>    
                </ProductCreateEditForm>
            </div>
        );
    }

}

function EditProductPage(props:EditProductRouteProps){
    
    return <Layout title="Edit Product">
        <EditProduct id={props.id} ></EditProduct>
    </Layout>

}

export const getServerSideProps: GetServerSideProps<EditProductRouteProps> = async (context) =>{
    let id = '';
    
    if(context.params)
    {
        const baca = context.params['id']

        if(baca && typeof(baca) === 'string')
        {
            id = baca ; 
        }
    }

    return{
        props:{
            id: id
        }
    }
}


export default EditProductPage;