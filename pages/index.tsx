import { observer } from 'mobx-react';
import { action, computed, makeObservable, observable } from 'mobx';
import React, { useState } from 'react';
import Link from 'next/link';
import { Layout } from './shared/Layout'

// let x = 5;

// function clickMe(){
//     x++;
//     console.log('hello ' + x);
// }

// function Index(){
//     return(
//         <div>
//             Hello World!
//             <button type = "button" onClick = {clickMe}>{x}</button>
//         </div>
//     );
// }


const Index: React.FunctionComponent<{}> = () => {
    // useState mereturn sebuah tuple, dimana tuple itu adalah setter getter
    const [count, setCount] = useState(0);
    const [text, setText] = useState('');

    return(
        <div>
            <p>
                {text}
            </p>
            <p>
                <input value = {text} onChange = {e => setText(e.target.value)}></input>
            </p>
            <button className = "btn btn-primary" onClick = {e => setCount(count+1)} >
                {count}
            </button>
        </div>
    );
}

// class Index extends React.Component<{}>{
//     constructor(props){
//         super(props);
//         makeObservable(this, {
//             x: observable,
//             clickMe: action,
//             y: computed
//         });
//     }

//     x = 1; //State
    
//     clickMe = () =>{ //Function yang ngerubah state
//         this.x++;
//     }

//     get y(): number{
//         return this.x * 2;
//     }

//     render(){
//         return(
//             <div>
//                 Hello World!
//                 <button type = "button" onClick={this.clickMe}>{this.x}</button>
//                 <div>{this.y}</div>

//                 <p>
//                     <Link href = '/todo'>
//                         <a>Go to todo list</a>
//                     </Link>
//                 </p>
//             </div>
//         );
//     }
// }

const IndexWithObserver = observer(Index);


export default function IndexPage(){
    return (
        <Layout title = "Home">
            <IndexWithObserver></IndexWithObserver>
        </Layout>
    )
};