import { faArrowLeft, faChevronUp, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import React from "react";
import Swal from "sweetalert2";
import { CustomerClient } from "../../api/shop-api";
import { Layout } from "../shared/Layout";

class CreateCustomer extends React.Component<{}, {
    form: {
        name: string,
        email: string
    },
    errors: {
        name: string,
        email: string
    },
    dirty: {
        name: boolean,
        email: boolean
    },
    busy: boolean
}> {

    constructor(props) {
        super(props);
        this.state = {
            form: {
                name: '',
                email: ''
            },
            errors: {
                name: '',
                email: ''
            },
            dirty: {
                name: false,
                email: false,
            },
            busy: false
        }
    }

    onNameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        const form = this.state.form;
        form.name = e.target.value;

        const dirty = this.state.dirty;
        dirty.name = true;

        this.setState({
            form: form,
            dirty: dirty
        });

        this.validate();
    }

    onEmailChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        const form = this.state.form;
        form.email = e.target.value;

        const dirty = this.state.dirty;
        dirty.email = true;

        this.setState({
            form: form,
            dirty: dirty
        });

        this.validate();
    }

    async validate() {
        const errors = {
            name: '',
            email: ''
        };

        // harusnya ada librarynya untuk validasi biar lebih enak...
        if (!this.state.form.name) {
            errors.name = 'Name field is required.';
        }
        if (!this.state.form.email) {
            errors.email = 'Email field is required.';
        } else {
            const hasAt = this.state.form.email.indexOf('@');
            if (hasAt === -1) {
                errors.email = 'Email field contains non-valid email.';
            }
        }

        await new Promise<void>((ok, reject) => {
            this.setState({
                errors: errors
            }, () => {
                ok();
            });
        });
    }

    hasErrorClassName(hasError: string | boolean, isDirty: boolean) {
        if (isDirty) {
            if (hasError) {
                return 'is-invalid';
            } else {
                return 'is-valid';
            }
        }

        return '';
    }

    onSubmit = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        await this.validate();

        let hasError = false;
        for (let key in this.state.errors) {
            if (this.state.errors[key]) {
                hasError = true;
            }
        }

        const dirty = this.state.dirty;

        for (let key in dirty) {
            dirty[key] = true;
        }

        this.setState({
            dirty: dirty
        });

        if (hasError) {
            return;
        }

        const form = this.state.form;

        this.setState({
            busy: true
        });

        try {
            const client = new CustomerClient('https://localhost:44324');
            await client.post({
                name: form.name,
                email: form.email
            });
        } catch (error) {
            console.error(error);
            Swal.fire({
                title: 'Submit Failed',
                text: 'An error has occurred. Please try again or contact an administrator',
                icon: 'error'
            });
        }
        finally {
            this.setState({
                busy: false
            });
        }

        this.setState({
            form: {
                name: '',
                email: ''
            },
            errors: {
                name: '',
                email: ''
            },
            dirty: {
                name: false,
                email: false
            }
        })

        Swal.fire({
            title: 'Success!',
            text: 'Successfully created new customer: ' + form.name,
            icon: 'success'
        });
    }

    getSubmitButtonIcon() {
        if (this.state.busy) {
            return <FontAwesomeIcon icon={faSpinner} pulse></FontAwesomeIcon>
        } else {
            return <FontAwesomeIcon icon={faChevronUp}></FontAwesomeIcon>
        }
    }

    render() {
        return <div>
            <h1>Create Customer</h1>
            <p>
                <Link href="/customer">
                    <a>
                        <span className="me-2">
                            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        </span>
                    Return to index
                </a>
                </Link>
            </p>
            <form onSubmit={this.onSubmit}>
                <fieldset disabled={this.state.busy}>
                    <div className="mb-3">
                        <label className="fw-bold" htmlFor="name">Name</label>
                        <input id="name" className={'form-control ' + this.hasErrorClassName(this.state.errors.name, this.state.dirty.name)}
                            value={this.state.form.name} onChange={this.onNameChanged}></input>
                        {this.state.errors.name && <span className="text-danger small">{this.state.errors.name}</span>}
                    </div>
                    <div className="mb-3">
                        <label className="fw-bold" htmlFor="email">Email</label>
                        <input id="email" className={'form-control ' + this.hasErrorClassName(this.state.errors.email, this.state.dirty.email)}
                            value={this.state.form.email} onChange={this.onEmailChanged}></input>
                        {this.state.errors.email && <span className="text-danger small">{this.state.errors.email}</span>}
                    </div>
                    <div className="mb-3">
                        <button type="submit" className="btn btn-primary">
                            <span className="me-2">
                                {this.getSubmitButtonIcon()}
                            </span>
                            Submit
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>;
    }
}

export default function CreateCustomerPage() {
    return <Layout title="Create Customer">
        <CreateCustomer></CreateCustomer>
    </Layout>;
}

