import React from 'react';
import Link from 'next/link';
import { GetServerSideProps } from 'next';
import { CustomerClient, CustomerListItems } from "../../../api/shop-api";
import { Router, useRouter, withRouter } from 'next/router';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faChevronUp, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { Layout } from "../../shared/Layout";
import Swal from 'sweetalert2';

/**
 * How this edit page works:
 * 1. Access this edit page with the supplied URL route parameter.
 * 2. Since you provides getServerSideProps, Next.js will determine that this page requires a blocking data requirements, so you can obtain the URL route.
 * 3. Render this page's included components (from EditCustomerPage -> Layout -> EditCustomer -> etc....).
 */

class EditCustomer extends React.Component<{
    id: string | undefined
}, {
    id: string,
    form: {
        name: string,
        email: string
    },
    errors: {
        name: string,
        email: string
    },
    dirty: {
        name: boolean,
        email: boolean,
    },
    busy: boolean
}>{
    constructor(props) {
        super(props);
        
        // console.log('Constructing...');
        // console.log('Props ID: ', props.id);

        this.state = {
            id: props.id,
            form: {
                name: '',
                email: ''
            },
            errors: {
                name: '',
                email: ''
            },
            dirty: {
                name: false,
                email: false,
            },
            busy: false
        }
    }
    
    async componentDidMount() {
        await this.loadCustomerData();
    }

    loadCustomerData = async () => {
        const client = new CustomerClient('https://localhost:44324');

        const data = await client.get(this.state.id);

        this.setState({
            form: {
                name: data.name ?? '',
                email: data.email ?? ''
            }
        });
    }

    onSubmit = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        await this.validate();

        let hasError = false;
        for (let key in this.state.errors) {
            if (this.state.errors[key]) {
                hasError = true;
            }
        }

        const dirty = this.state.dirty;

        for (let key in dirty) {
            dirty[key] = true;
        }

        this.setState({
            dirty: dirty
        });

        if (hasError) {
            return;
        }

        const form = this.state.form;

        this.setState({
            busy: true
        });

        try {
            const client = new CustomerClient('https://localhost:44324');
            // The method should be named update....
            await client.post2(this.state.id, {
                name: form.name,
                email: form.email
            });
        } catch (error) {
            console.error(error);
            Swal.fire({
                title: 'Submit Failed',
                text: 'An error has occurred. Please try again or contact an administrator',
                icon: 'error'
            });
        }
        finally {
            this.setState({
                busy: false
            });
        }

        this.setState({
            dirty: {
                name: false,
                email: false
            }
        })

        Swal.fire({
            title: 'Success!',
            text: 'Successfully updated customer: ' + form.name,
            icon: 'success'
        });
    }

    hasErrorClassName(hasError: string | boolean, isDirty: boolean) {
        if (isDirty) {
            if (hasError) {
                return 'is-invalid';
            } else {
                return 'is-valid';
            }
        }

        return '';
    }

    onNameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        const form = this.state.form;
        form.name = e.target.value;

        const dirty = this.state.dirty;
        dirty.name = true;

        this.setState({
            form: form,
            dirty: dirty
        });

        this.validate();
    }

    onEmailChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        const form = this.state.form;
        form.email = e.target.value;

        const dirty = this.state.dirty;
        dirty.email = true;

        this.setState({
            form: form,
            dirty: dirty
        });

        this.validate();
    }

    async validate() {
        const errors = {
            name: '',
            email: ''
        };

        // harusnya ada librarynya untuk validasi biar lebih enak...
        if (!this.state.form.name) {
            errors.name = 'Name field is required.';
        }
        if (!this.state.form.email) {
            errors.email = 'Email field is required.';
        } else {
            const hasAt = this.state.form.email.indexOf('@');
            if (hasAt === -1) {
                errors.email = 'Email field contains non-valid email.';
            }
        }

        await new Promise<void>((ok, reject) => {
            this.setState({
                errors: errors
            }, () => {
                ok();
            });
        });
    }

    getSubmitButtonIcon() {
        if (this.state.busy) {
            return <FontAwesomeIcon icon={faSpinner} pulse></FontAwesomeIcon>
        } else {
            return <FontAwesomeIcon icon={faChevronUp}></FontAwesomeIcon>
        }
    }

    render() {
        return <div>
            <h1>Edit Customer</h1>
            <p>
                <Link href="/customer">
                    <a>
                        <span className="me-2">
                            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        </span>
                    Return to index
                </a>
                </Link>
            </p>
            <form onSubmit={this.onSubmit}>
                <fieldset disabled={this.state.busy}>
                    <div className="mb-3">
                        <label className="fw-bold" htmlFor="name">Name</label>
                        <input id="name" className={'form-control ' + this.hasErrorClassName(this.state.errors.name, this.state.dirty.name)}
                            value={this.state.form.name} onChange={this.onNameChanged}></input>
                        {this.state.errors.name && <span className="text-danger small">{this.state.errors.name}</span>}
                    </div>
                    <div className="mb-3">
                        <label className="fw-bold" htmlFor="email">Email</label>
                        <input id="email" className={'form-control ' + this.hasErrorClassName(this.state.errors.email, this.state.dirty.email)}
                            value={this.state.form.email} onChange={this.onEmailChanged}></input>
                        {this.state.errors.email && <span className="text-danger small">{this.state.errors.email}</span>}
                    </div>
                    <div className="mb-3">
                        <button type="submit" className="btn btn-primary">
                            <span className="me-2">
                                {this.getSubmitButtonIcon()}
                            </span>
                            Submit
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>;
    }
}

/**
 * Function component for edit customer page.
 * @param param0 
 * @returns 
 */
function EditCustomerPage() {
    // Use useRouter to obtain the URL parameters.
    // You can also use withRouter instead.
    // Reference: https://nextjs.org/docs/api-reference/next/router#userouter.
    const router = useRouter();

    // To obtain the URL route parameter name, the object name must have the same name as the .tsx file name of this page.
    // In this case, our .tsx file is named [id].tsx, so we must bind the router.query object with name 'id'.
    const { id } = router.query;

    console.log('id parameter', id);

    return <Layout title="Edit Customer">
        <EditCustomer id={id?.toString()}></EditCustomer>
    </Layout>;
}

// The getServerSideProps must be present in the page if we want to acquire the URL route parameter value on the initial page load due to Automatic Static Optimization.
// Reference: https://nextjs.org/docs/advanced-features/automatic-static-optimization.
/**
 * If you export an async function called getServerSideProps from a page, Next.js will pre-render this page on each request using the data returned by getServerSideProps.
 * Reference: https://nextjs.org/docs/basic-features/data-fetching#getserversideprops-server-side-rendering.
 * @param context 
 */
export const getServerSideProps: GetServerSideProps = async (context) => {
    return {
        props: {
        }
    };
}

export default EditCustomerPage;