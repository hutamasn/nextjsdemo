import { Layout } from "../shared/Layout";
import React from "react";CustomerClient
import Link from "next/link";
import Swal, { SweetAlertResult } from "sweetalert2";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faPlus, faThumbsDown, faTimes } from '@fortawesome/free-solid-svg-icons';
import { CustomerClient, CustomerListItems } from "../../api/shop-api";
import { UserManagerFactory } from "../../services/UserManagerFactory";
import { CustomerClientWithAuth } from "../../services/NSwagWithAuthFactory";

const DeleteCustomerButton: React.FunctionComponent<{
    customerID: string,
    name: string | undefined,
    onDeleted?: () => void
}> = (props) => {

    const onClick = async () => {
        const confirm = await Swal.fire<SweetAlertResult>({
            title: 'Confirm delete?',
            text: `Delete customer ${props.name}? This action cannot be undone.`,
            icon: 'warning',
            confirmButtonColor: '#dc3545',
            showCancelButton: true,
            confirmButtonText: 'Delete'
        });

        if (confirm.isConfirmed === false) {
            return;
        }

        const client = new CustomerClient('https://localhost:44324');
        await client.delete(props.customerID);

        Swal.fire({
            toast: true,
            timerProgressBar: true,
            timer: 5000,
            showConfirmButton: false,
            position: 'bottom-right',
            icon: 'success',
            title: 'Successfully deleted customer: ' + props.name
        });

        if (props.onDeleted) {
            props.onDeleted();
        }
    };

    return (
        <button type="button" onClick={onClick} className="btn btn-danger btn-sm">
            <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
        </button>
    );
};
//Edit customer button component

const EditCustomerButton: React.FunctionComponent<{
    customerID: string,
}> = (props) => {
    return (
        <Link href = {`customer/edit/${props.customerID}`}>
            <a href = "customer/edit" className="btn btn-info btn-sm">
                <FontAwesomeIcon icon={faEdit}></FontAwesomeIcon>
            </a>
        </Link>
    );
};

const CustomerListItemRows: React.FunctionComponent<{
    customers: CustomerListItems[],
    onChanged?: () => void
}> = (props) => {
    const rows = props.customers.map(Q =>
        <tr key={Q.customerID}>
            <td>{Q.customerID}</td>
            <td>{Q.name}</td>
            <td>{Q.email}</td>
            
            <td>
                <EditCustomerButton customerID = {Q.customerID}></EditCustomerButton>
                <DeleteCustomerButton customerID={Q.customerID} name={Q.name} onDeleted={props.onChanged}></DeleteCustomerButton>
            </td>
        </tr>
    );

    return <tbody>{rows}</tbody>;
}

class Customer extends React.Component<{}, {
    customers: CustomerListItems[]
}> {
    constructor(props) {
        super(props);
        this.state = {
            customers: []
        };
    }

    async componentDidMount() {
        await this.reloadCustomerData();
    }

    reloadCustomerData = async () => {
        const userManager = UserManagerFactory();
        const user = await userManager.getUser()

        if(user){
            const client = CustomerClientWithAuth(user);
            const data = await client.getAll();
            this.setState({
                customers: data
            });
        } 
    }

    render() {
        return (
            <div>
                <h1>Manage Customer</h1>
                <p>
                    <Link href="/customer/create">
                        <a className="btn btn-primary">
                            <span className="me-2">
                                <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                            </span>
                            Add New Customers
                        </a>
                    </Link>
                </p>
                <table className="table table-hover table-striped">
                    <thead className="bg-dark text-light">
                        <tr>
                            <th>Customer ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                    </thead>
                    <CustomerListItemRows customers={this.state.customers} onChanged={this.reloadCustomerData}>
                    </CustomerListItemRows>
                </table>
            </div>
        );
    }
}

export default function CustomerPage() {
    return (
        <Layout title="Customer">
            <Customer></Customer>
        </Layout>
    );
}
