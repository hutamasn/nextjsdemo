import React, { useEffect, useState } from "react";
import Head from 'next/head';
import Link from "next/link";
import {useRouter} from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignInAlt, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { UserManagerFactory } from "../../services/UserManagerFactory";
import  tryLoginUsingAccelistSSO  from "../account/login" ;

function GetNavigationLinkClassName(active: boolean){
    if(active){
        return "nav-link-active";
    } else{
        return "nav-link";
    }
}

function GetNavigationLinkAriaCurrent(active: boolean){
    if(active){
        return "page";
    } else{
        return undefined;
    }
}

const NavigationLink: React.FunctionComponent<{
    href: string
}> = (props) =>{
    const router = useRouter();
    const active = (router.pathname === props.href);
    return (
            <li className="nav-item">
                <Link href = {props.href}>
                    <a className={GetNavigationLinkClassName(active)} aria-current={GetNavigationLinkAriaCurrent(active)}>
                        {props.children}
                    </a>
                </Link>
            </li>
    );
};

const Dropdown: React.FunctionComponent<{}> = () => {

    const[ready,setReady] = useState(false);
    const [fullname, setFullName] = useState('');
    const getUserData = async () => {
        const userManager = UserManagerFactory();
        const user = await userManager.getUser();
        setFullName(user?.profile?.name ?? '');
        setReady(true);
    }

    useEffect(() => {
        getUserData();
    }, []);

    if(!ready){
        return <div></div>
    }

    if (!fullname) {
        return (
            <ul className="navbar-nav ms-auto">
                <li className="nav-item">
                    <a className="btn btn-primary btn-sm" onClick={tryLoginUsingAccelistSSO}>
                        <FontAwesomeIcon className="me-2" icon={faSignInAlt}></FontAwesomeIcon>
                        Sign In
                    </a>
                </li>
            </ul>

        )
    }
    return (
        <ul className="navbar-nav ms-auto">
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Welcome,{fullname}
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li>
                        <Link href="/account/logout">
                            <a>Sign Out</a>
                        </Link>
                    </li>
                </ul>
            </li>
        </ul>
    );
}

const NavigationBar: React.FunctionComponent<{}> = () =>{
    return(
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
            <Link href = "/">
                <a className = "navbar-brand">3+1</a>
            </Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <NavigationLink href = "/">Home</NavigationLink>
              <NavigationLink href = "/todo">Todo</NavigationLink>
              <NavigationLink href = "/customer">Customer</NavigationLink>
              <NavigationLink href = "/product">Product</NavigationLink>
            </ul>
            <Dropdown></Dropdown>
          </div>
        </div>
      </nav>
    )
}

export class Layout extends React.Component<{
    title: string;
}>{
    render(){
        return(
            <div>
                <Head>
                    <meta charSet="utf-8"/>
                    <meta name = "viewport" content = "width-device-width, initial-scale=1"/>
                    <title>{this.props.title} - Belajar React</title>
                </Head>
                <header>
                    <NavigationBar></NavigationBar>
                </header>
                <main className = "mb-5 mt-4 container">
                    {this.props.children}
                </main>
                <footer>
                    <script src = "bootstrap.bundle.min.js"></script>
                </footer>
            </div>
        );
    }
}


